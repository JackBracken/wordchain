# frozen_string_literal: true

require 'spec_helper'
require 'rspec'
require 'wordchain'

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.order = 'random'
end

describe WordChain do
  it 'loads the default library when passed no args' do
    expect(WordChain.new.file).to be '/usr/share/dict/words'
  end

  it 'loads the given dictionary file' do
    file = '/foo/bar'
    expect(WordChain.new(file).file).to be file
  end

  it 'prints all wordchains in a dictionary' do
    dict = %w(foo bar baz)
    allow(File).to receive(:readlines).with('foo', chomp: true).and_return(dict)

    wc = WordChain.new('foo')
    expect { wc.search 'bz' }.to output("baz\n").to_stdout
    expect { wc.search 'zb' }.to output('').to_stdout
  end
end

describe String do
  it 'can recognise a wordchain' do
    TESTS = [
      { str: 'abcdef', sub: 'ace', res: true },
      { str: 'bcdefa', sub: 'ace', res: false },
      { str: 'efabcd', sub: 'ace', res: false },
      { str: 'fabcde', sub: 'ace', res: true }
    ].freeze

    TESTS.each do |t|
      expect(t[:str].chain?(t[:sub])).to be t[:res]
    end
  end
end
