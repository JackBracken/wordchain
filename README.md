# Readme

Prints out all of the wordchains in a given dictionary file.

A wordchain is a terrible name for any string which contains all of
the characters of a given substring in the same order (but not
necessarily) grouped together.

For example, wordchains for the substring 'abcd' include:

- **ab**ra**c**a**d**abra
- **ab**stra**c**te**d**ly
- sw**a**sh**b**u**c**kler**d**om

The dictionary file should be formatted similarly to the one in
`/usr/share/dict/words` on \*nix systems.

# Uses

Almost none.

I wrote this to help generate ideas for project names, specifically
a gameboy emulator written in rust. Passing in 'rsgb' for 'rust
gameboy' yields:

- contrasuggestible
- hypersuggestibility
- impervestigable
- intransgressible
- predisagreeable
- transgressible

Passing in 'gbrs' for the reverse literally yields gibberish. :D

