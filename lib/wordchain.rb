# frozen_string_literal: true

class WordChain
  DEFAULT_DICT_FILE = '/usr/share/dict/words'

  attr_accessor :words
  attr_reader :file

  def initialize(file = nil)
    @file = if file.nil?
              DEFAULT_DICT_FILE
            else
              file
            end
  end

  def search(chain)
    c = chain.chomp
    File.readlines(@file, chomp: true).each do |l|
      puts l if l.chain? c
    end
  end
end

class String
  def chain?(substr)
    return true if empty? && substr.empty?
    return false if length < substr.length
    return false if length.zero?
    return true if substr.empty?

    c = substr[0]
    return false if index(c).nil?
    self[index(c) + 1..-1].chain? substr[1..-1]
  end
end
